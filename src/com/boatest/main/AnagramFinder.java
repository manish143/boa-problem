package com.boatest.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Manish Singh
 * The AnagramFinder class first Maps all alphabets to prime numbers in the array primes. This is one time activity.
 * After that Dictionary words are converted to Word Vs Anagrams Map. Key in Map is hashcode generated via multiplying prime numbers
 * corresponding to each alphabet in the word. This way all anagrams will have same hashcode.
 * 
 * Complexity wise, getPrimeNumbersForAlphabets() is called only once. It finds first 52 prime numbers(26 * 2) and Maps them to (A to Z and a to z).
 * complexity will be ~ O(n*rootOf(n)) in this case (52*rootOf(52)), that is pseudo polynomial as in isPrime() method we check only till half of number for prime test..
 * Making Dictionary map is constant time, as generating hash for word is constant time operation. 
 * Finding Anagrams too is constant time O(1).
 * So overall the main operations are constant time.
 * 
 * Space complexity will be O(no of words in dictionary). Prime no array is of length 122 which is again constant for large word set
 * and can be ignored.
 */
public class AnagramFinder {
	
	int[] primes = getPrimeNumbersForAlphabets();
	
	public static void main(String[] args) {
		List<String> dictionary = new ArrayList<>();
		dictionary.add("a");
		dictionary.add("bat");
		dictionary.add("tab");
		dictionary.add("vote");
		dictionary.add("veto");
		dictionary.add("cat");
		dictionary.add("act");
		dictionary.add("tac");
		dictionary.add("eat");
		dictionary.add("ate");
		dictionary.add("tea");
		
		AnagramFinder af = new AnagramFinder();
		Map<Integer, List<String>> wordToAnagramMap = af.findAllAnagramsInDictionary(dictionary.iterator());
		System.out.println(af.findAllAnagramsForWord("a", wordToAnagramMap));
		System.out.println(af.findAllAnagramsForWord("bat", wordToAnagramMap));
		System.out.println(af.findAllAnagramsForWord("vote", wordToAnagramMap));
		System.out.println(af.findAllAnagramsForWord("cat", wordToAnagramMap));
		System.out.println(af.findAllAnagramsForWord("eat", wordToAnagramMap));
		System.out.println(af.findAllAnagramsForWord("ball", wordToAnagramMap));
	}

	public Map<Integer, List<String>> findAllAnagramsInDictionary(Iterator<String> dictionary) {
		if(dictionary == null) {
			return null;
		}
		Map<Integer, List<String>> wordToAnagramMap = new HashMap<>();
		while(dictionary.hasNext()) {
			String word = dictionary.next();
			Integer key = findKey(word);
			if(wordToAnagramMap.containsKey(key)) {
				List<String> anagrams = wordToAnagramMap.get(key);
				if(!anagrams.contains(word)) {
					anagrams.add(word);
				}
			} else {
				List<String> anagrams = new ArrayList<>();
				anagrams.add(word);
				wordToAnagramMap.put(key, anagrams);
			}
		}
		return wordToAnagramMap;
	}
	
	public List<String> findAllAnagramsForWord(String word, Map<Integer, List<String>> wordToAnagramMap) {
		Integer key = findKey(word);
		if(wordToAnagramMap.containsKey(key)) {
			List<String> anagrams =  wordToAnagramMap.get(key);
			List<String> resultList = new ArrayList<>();
			for(String anagram : anagrams) {
				if(!anagram.equals(word)) {
					resultList.add(anagram);
				}
			}
			return resultList;
		}
		return null;
	}

	private Integer findKey(String word) {
		char[] chars = word.toCharArray();
		int key = 1;
		for(int i = 0 ; i < chars.length ; i++) {
			key *= primes[chars[i]];
		}
		return key;
	}
	
	private static int[] getPrimeNumbersForAlphabets() {
		int[] primes = new int[122];
		int counter = 65;
		int i = 2;
		for (; counter <= 90; i++) {
	        if (isPrime(i)) {
	        	primes[counter++] = i;
	        }
	    }
		counter = 97;
		for (; counter < 122; i++) {
	        if (isPrime(i)) {
	        	primes[counter++] = i;
	        }
	    }
		return primes;
	}
	
	private static boolean isPrime(int check) {
	    for (int i = 2; i < check/2; i++) {
	        if (check % i == 0) {
	            return false;
	        }
	    }
	    return true;
	}
}
